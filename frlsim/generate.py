import frontmatter
import hashlib
from jinja2 import Markup
from markdown import markdown
import os
import subprocess

class Document:
	def __init__(self, code=None, metadata=None, content=None):
		self.code = code
		self.metadata = metadata
		self.content = content
	
	@property
	def article(self):
		if 'article' in self.metadata:
			return self.metadata['article']
		return int(self.code[4:5])
	
	@property
	def issue(self):
		if 'issue' in self.metadata:
			return self.metadata['issue']
		return int(self.code[0:2])

	@property
	def author(self):
		if 'author' in self.metadata:
			return self.metadata['author']
		return 'Unknown'

	@property
	def category(self):
		if 'category' in self.metadata:
			return self.metadata['category']
		return 'Unknown'
	
	@property
	def formats(self):
		return [filename.split('.')[1] for filename in sorted(os.listdir(os.path.join('data', self.FOLDER_NAME))) if filename.startswith(self.code)]
	
	@property
	def can_render(self):
		if self.content:
			return True
		if 'html' in self.autoconverted:
			return True
		if 'md' in self.autoconverted:
			return True
		if 'txt' in self.autoconverted:
			return True
		return False
	
	def render(self):
		if self.content:
			return Markup(markdown(self.content))
		if 'html' in self.autoconverted:
			with open(self.autoconverted['html'], 'r') as f:
				return Markup(f.read())
		if 'md' in self.autoconverted:
			with open(self.autoconverted['md'], 'r') as f:
				return Markup(markdown(f.read()))
		if 'txt' in self.autoconverted:
			with open(self.autoconverted['txt'], 'r') as f:
				return Markup('<pre style="max-height: 80vh; overflow: scroll;">' + f.read() + '</pre>')
		return None
	
	def autoconvert(self):
		formats = self.formats
		if 'docx' in formats:
			args = ['pandoc', os.path.join('data', self.FOLDER_NAME, self.code + '.docx'), '-t', 'html5', '--self-contained']
			out_format = 'html'
		elif 'odt' in formats:
			args = ['pandoc', os.path.join('data', self.FOLDER_NAME, self.code + '.odt'), '-t', 'html5', '--self-contained']
			out_format = 'html'
		elif 'pdf' in formats:
			args = ['pdftotext', '-layout', os.path.join('data', self.FOLDER_NAME, self.code + '.pdf'), '-']
			out_format = 'txt'
		else:
			return None
		
		m = hashlib.sha256()
		m.update(' '.join(args).encode('utf-8'))
		out_file = os.path.join('cache', self.code + '.' + m.hexdigest() + '.' + out_format)
		os.makedirs(os.path.dirname(out_file), exist_ok=True)
		
		if os.path.exists(out_file):
			print('Using cached {} for {}'.format(out_file, ' '.join(args)))
		else:
			print('Running {}'.format(' '.join(args)))
			result = subprocess.run(args, capture_output=True, encoding='utf-8')
			with open(out_file, 'w') as f:
				print(result.stdout, file=f)
		
		self.autoconverted[out_format] = out_file
		
		return out_format
	
	@classmethod
	def from_file(cls, code):
		data = frontmatter.load(os.path.join('data', cls.FOLDER_NAME, code + '.md'))
		return cls(code, data.metadata, data.content.strip())

class Newsletter(Document):
	FOLDER_NAME = 'newsletters'
	
	def __init__(self, code=None, metadata=None, content=None):
		super().__init__(code, metadata, content)

def generate(core):
	# Read Newsletters
	
	newsletters = []
	for filename in sorted(os.listdir(os.path.join('data', 'newsletters'))):
		if filename.endswith('.md'):
			newsletter = Newsletter.from_file(filename[:-3])
			newsletters.append(newsletter)
	
	newsletters_bycode = {newsletter.code: newsletter for newsletter in newsletters}
	
	# Indexes
	
	core.render('index.html', 'frlsim/index.html')
	core.render('about/', 'frlsim/about.html')
	core.render('privacy/', 'frlsim/privacy.html')
	core.render('take-action/', 'frlsim/take-action.html')
	core.render('take-action/pledge/', 'frlsim/pledge.html')
	core.render('take-action/emailsignup/', 'frlsim/emailsignup.html')
	core.render('take-action/join/', 'frlsim/join.html')
	core.render('may21/', 'frlsim/may21.html')
	core.render('school/', 'frlsim/schools.html')
	core.render('thankyou/', 'frlsim/thankyou.html')
	core.render('contact/', 'frlsim/contact.html')

	core.render('newsletters/', 'frlsim/newsletters.html', newsletters=newsletters)
	
	# Generate Individualised School Pages
	
	schools = ["bcc","ccc","mac","nrss","smg","spb","bhs","chs","gths","hsh","lmhs","shs","ths","wbhs","wwhs","whs","btac","hcs","gram","san","sfx","spcc","spx","cjes","cwal","cwar","fghs","hspa","khs","lhs","mhs","nhs","hvgs"]
	
	for school in schools:
		core.render('school/{}/'.format(school), 'frlsim/school.html', school=school)
	
	# Generate newsletters
	
	for newsletter in newsletters:
		core.render('newsletter/{}/'.format(newsletter.code), 'frlsim/newsletter_view.html', newsletter=newsletter, newsletters_bycode=newsletters_bycode)
